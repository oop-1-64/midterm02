/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.midterm02;
import java.util.Scanner;

/**
 *
 * @author domem
 */
public class Perennial extends Tree{    //just only perrennial type have annualRing.!! 
    Scanner ip = new Scanner(System.in); //import Scanner cause need input annualRing from keyboard.
    private int annualRing; 
    public Perennial (int height){
        super(height);
        this.annualRing = annualRing;
    }
    
    @Override
    public void printType() {   //override from tree class
        System.out.println("This tree is Perennial"+ " (height: "+height+")");
    }
    
    public void checkAge(){     //1 annualRing = 1 year old.
        System.out.println("How many annualRing ? = ");
        annualRing = ip.nextInt();      //input form keyboard
        System.out.println("Age of this tree is "+annualRing+" years old");
    }
    
}
