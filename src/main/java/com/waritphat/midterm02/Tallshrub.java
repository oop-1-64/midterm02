/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.midterm02;

/**
 *
 * @author domem
 */
public class Tallshrub extends Tree {

    public Tallshrub(int height) {
        super(height);
    }
    
    @Override
    public void printType() {   //override from tree class
        System.out.println("This tree is Tallshrub"+ " (height: "+height+")");
    }

}
