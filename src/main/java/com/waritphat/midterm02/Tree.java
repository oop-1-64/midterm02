/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.midterm02;

/**
 *
 * @author domem
 */
public class Tree {

    protected int height;
    protected int count;
    protected char light;


    public Tree(int height) {
        this.height = height;
        this.count = count;
        this.light = 's';
    }

    public boolean checkType() { //Check Type of trees : [Groundcover, Shortshrub, Smallshrub, Middleshrub, Tallshrub, Perennial] 
        if (height <= 30) {         //This tree is Groundcover.
            Groundcover ground01 = new Groundcover(height);
            ground01.printType();   //override
        } else if (height <= 45) {  //This tree is Shortshrub.
            Shortshrub short01 = new Shortshrub(height);
            short01.printType();    //override
        } else if (height <= 90) {  //This tree is Smallshrub.
            Smallshrub small01 = new Smallshrub(height);
            small01.printType();    //override
        } else if (height <= 180) { //This tree is Middleshrub.
            Middleshrub mid01 = new Middleshrub(height);
            mid01.printType();            //override
        } else if (height <= 240) { //This tree is Tallshrub.
            Tallshrub tall01 = new Tallshrub(height);
            tall01.printType();     //override
        } else {                    //This tree is Perennial.
            Perennial per01 = new Perennial(height);
            per01.printType();      //override
            per01.checkAge();       //call checkAge at Perrennial class
        }
        return true;
    }
    
    // **the tree can't higher than its type.**
    
    public void growUP() { //overload
        canWater(); //Before water the trees, must check height is the point of max? 
        System.out.println("higher: "+height+" by water");
        
    }

    public boolean canWater() { //check can grow up and height+2 by water
        if (height+2 <=30) {     
            height = height + 2;
        } else if (height > 30 && height + 2 <= 45) {
            height = height + 2;
        } else if (height > 45 && height + 2 <= 90) {
            height = height + 2;
        } else if (height > 90 && height + 2 <= 180) {
            height = height + 2;
        } else if (height > 180 && height + 2 <= 240) {
            height = height + 2;
        } else if (height >240){
            height = height + 2;
        }
        return false;
    }

    public void growUP(int count) { //overload
        canManure(count);   //Before manure the trees, must check height is the point of max? 
        System.out.println("higher: "+height+" by "+ count +" manure");
    }

    public int canManure(int count) {   //check can grow up and height + count of manure by manure
        if (height + count <= 30) {
            height = height + count;
        } else if (height > 30 && height + count <= 45) {
            height = height + count;
        } else if (height > 45 && height + count <= 90) {
            height = height + count;
        } else if (height > 90 && height + count <= 180) {
            height = height + count;
        } else if (height > 180 && height + count <= 240) {
            height = height + count;
        } else if (height>240){
            height = height + count;
        }
        return height;
    }

    public void growUP(char light) {
        canLight(); //Before give sunlight the trees, must check height is the point of max? 
        System.out.println("higher: "+height+" by light");
    }

    public int canLight() {         //check can grow up and height + 1 by sunlight.
        if (height + 1 <= 30) {
            height = height + 1;
        } else if (height > 30 && height + 1 <= 45) {
            height = height + 1;
        } else if (height > 45 && height + 1 <= 90) {
            height = height + 1;
        } else if (height > 90 && height + 1 <= 180) {
            height = height + 1;
        } else if (height > 180 && height + 1 <= 240) {
            height = height + 1;
        } else if (height>240){
            height = height + 1;
        }
        return height;
    }

    public void printType() { //Overriding
        System.out.println("This tree is type" + " (height: " + height + ")");
    }
    
    public void newline(){
        System.out.println("===============================");
    }
}
